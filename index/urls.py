#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behzadi<dani.behzi@ubuntu.com>, 2020

from django.urls import path
from . import views

urlpatterns = [
        path('', views.index, name='index'),
        path('<str:gender>/info/<int:pid>', views.info),
        ]
