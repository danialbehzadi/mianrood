from django.http import StreamingHttpResponse
from django.shortcuts import render, get_object_or_404
from json import dumps as jsonify
from .models import Male, Female

# Create your views here.
def index(request):
    males = Male.objects.all()
    return render(request, 'index/index.html', {'males': males})

def info(request, gender, pid):
    if gender == 'm':
        person = get_object_or_404(Male, pk=pid)
    elif gender=='f':
        person = get_object_or_404(Female, pk=pid)
    else:
        return StreamingHttpResponse('Bad Request', status=400)
    result = {
            'fname': person.first_name,
            'lname': person.last_name,
            'job': person.job,
            'nickname': person.nick_name,
            'father': person.father_id,
            'mother': person.mother_id,
            'gender': gender,
            }
    #result = result|children(person) #python3.9+
    result = {**result, **children(person)}  #python3.8-
    return StreamingHttpResponse(jsonify(result), status=200)

def children(person):
    sons = person.sons
    daughters = person.daughters
    result = {
            'sons': list(sons.values_list('id', flat=True)),
            'daughters': list(daughters.values_list('id', flat=True)),
            }
    return result
