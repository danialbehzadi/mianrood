from django.db import models

# Create your models here.
class Citizen (models.Model):
    first_name = models.CharField(blank=True, null=True, max_length=31)
    last_name = models.CharField(blank=True, null=True, max_length=31)
    nick_name = models.CharField(blank=True, null=True, max_length=31)
    job = models.CharField(blank=True, null=True, max_length=15)
    class Meta:
        abstract = True

    def __str__(self):
        if self.first_name and self.last_name:
            return '{} {}'.format(self.first_name, self.last_name)
        else:
            return '{}'.format(self.first_name)

class Male(Citizen):
    wife = models.ManyToManyField('Female', blank=True)
    father = models.ForeignKey('Male', on_delete=models.SET_NULL, blank=True, null=True, related_name='sons')
    mother = models.ForeignKey('Female', on_delete=models.SET_NULL, blank=True, null=True, related_name='sons')

class Female(Citizen):
    father = models.ForeignKey('Male', on_delete=models.SET_NULL, blank=True, null=True, related_name='daughters')
    mother = models.ForeignKey('Female', on_delete=models.SET_NULL, blank=True, null=True, related_name='daugthers')
